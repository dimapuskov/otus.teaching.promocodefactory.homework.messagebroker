﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.Core.Abstractions.Services
{
    public interface IEmploeeService
    {
        Task<IEnumerable<Employee>> GetEmployeesAsync();
        Task<Employee> GetEmployeeByIdAsync(Guid id);
        Task<Employee> UpdateAppliedPromocodesAsync(Guid id);
    }
}