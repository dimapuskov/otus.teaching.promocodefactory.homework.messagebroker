﻿using MassTransit;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Abstractions.Events;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class AppliedPromocodesConsumer : IConsumer<IReceivingPromocode>
    {
        private readonly IEmploeeService _emploeeService;
        private readonly ILogger<AppliedPromocodesConsumer> _logger;
        public AppliedPromocodesConsumer(ILogger<AppliedPromocodesConsumer> logger, IEmploeeService emploeeService)
        {
            _logger = logger;
            _emploeeService = emploeeService;
        }

        public async Task Consume(ConsumeContext<IReceivingPromocode> context)
        {
            if (!context.Message.PartnerManagerId.HasValue)
            {
                _logger.LogWarning("There is no PartnerManagerId");   
                return;
            }

            await _emploeeService.UpdateAppliedPromocodesAsync(context.Message.PartnerManagerId.Value);
        }
    }
}