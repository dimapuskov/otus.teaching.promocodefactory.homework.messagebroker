﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services
{
    public class EmploeeService : IEmploeeService
    {
        private readonly IRepository<Employee> _employeeRepository;
        public EmploeeService(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public async Task<Employee> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            return employee;
        }

        public async Task<IEnumerable<Employee>> GetEmployeesAsync()
        {
            var employers=  await _employeeRepository.GetAllAsync();

            return employers;
        }

        public async Task<Employee> UpdateAppliedPromocodesAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            

            if (employee == null)
                return null;

            employee.AppliedPromocodesCount++;

            await _employeeRepository.UpdateAsync(employee);

            return employee;
        }
    }
}