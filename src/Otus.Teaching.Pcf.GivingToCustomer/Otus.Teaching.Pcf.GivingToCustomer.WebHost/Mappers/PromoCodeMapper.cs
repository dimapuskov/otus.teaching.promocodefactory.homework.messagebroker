﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Abstractions.Events;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

 namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers
{
    public class PromoCodeMapper
    {
        public static PromoCode MapFromModel(GivePromoCodeRequest request, Preference preference, IEnumerable<Customer> customers) {

            var promocode = new PromoCode();
            promocode.Id = request.PromoCodeId;
            
            promocode.PartnerId = request.PartnerId;
            promocode.Code = request.PromoCode;
            promocode.ServiceInfo = request.ServiceInfo;
           
            promocode.BeginDate = DateTime.Parse(request.BeginDate);
            promocode.EndDate = DateTime.Parse(request.EndDate);

            promocode.Preference = preference;
            promocode.PreferenceId = preference.Id;

            promocode.Customers = new List<PromoCodeCustomer>();

            foreach (var item in customers)
            {
                promocode.Customers.Add(new PromoCodeCustomer()
                {

                    CustomerId = item.Id,
                    Customer = item,
                    PromoCodeId = promocode.Id,
                    PromoCode = promocode
                });
            };

            return promocode;
        }

        public static IReceivingPromocode MapFromPromoCodeRequest(GivePromoCodeRequest request)
        {
            var result = new ReceivingPromocode();

            result.BeginDate = request.BeginDate;
            result.EndDate = request.EndDate;
            result.ServiceInfo = request.ServiceInfo;
            result.PreferenceId = request.PreferenceId;
            result.PromoCodeId = request.PromoCodeId;
            result.PromoCode = request.PromoCode;
            result.PartnerId = request.PartnerId;

            result.PartnerManagerId = null;
            
            return result;
        }

        public static GivePromoCodeRequest MapFromReceivingPromocode(IReceivingPromocode request)
        {
            var result = new GivePromoCodeRequest();

            result.BeginDate = request.BeginDate;
            result.EndDate = request.EndDate;
            result.ServiceInfo = request.ServiceInfo;
            result.PreferenceId = request.PreferenceId;
            result.PromoCodeId = request.PromoCodeId;
            result.PromoCode = request.PromoCode;
            result.PartnerId = request.PartnerId;

            return result;
        }
    }
}
