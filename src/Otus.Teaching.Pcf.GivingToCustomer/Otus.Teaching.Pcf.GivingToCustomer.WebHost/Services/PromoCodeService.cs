﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Pcf.Abstractions.Events;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services
{
    public class PromoCodeService : IPromocodeService
    {
        private readonly ILogger<PromoCodeService> _logger;

        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;
        public PromoCodeService(ILogger<PromoCodeService> logger, IRepository<PromoCode> repository,
                IRepository<Preference> preferencesRepository, IRepository<Customer> customersRepository)
        {
            _logger = logger;
            _promoCodesRepository = repository;
            _customersRepository = customersRepository;
            _preferencesRepository = preferencesRepository;
        }

        public async Task<IEnumerable<PromoCode>> GetPromocodesAsync()
        {
            var promocodes = await _promoCodesRepository.GetAllAsync();

            return promocodes;
        }

        public async Task<PromoCode> GivePromoCodesToCustomerAsync(IReceivingPromocode request)
        {
            //Получаем предпочтение по имени
            var preference = await _preferencesRepository.GetByIdAsync(request.PreferenceId);

            if (preference == null)
            {
                _logger.LogWarning($"There is no Preference, PreferenceID = {request.PreferenceId}");
                return null;
            }

            //  Получаем клиентов с этим предпочтением:
            var customers = await _customersRepository
                    .GetWhere(d => d.Preferences.Any(x =>
                            x.Preference.Id == preference.Id));

            PromoCode promoCode = PromoCodeMapper.MapFromModel(PromoCodeMapper.MapFromReceivingPromocode(request), preference, customers);

            await _promoCodesRepository.AddAsync(promoCode);

            return promoCode;
        }
    }
}