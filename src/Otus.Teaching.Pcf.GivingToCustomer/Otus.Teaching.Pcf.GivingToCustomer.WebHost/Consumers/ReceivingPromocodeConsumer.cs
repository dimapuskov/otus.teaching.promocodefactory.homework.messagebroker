﻿using MassTransit;
using Otus.Teaching.Pcf.Abstractions.Events;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers
{
    public class ReceivingPromocodeConsumer : IConsumer<IReceivingPromocode>
    {
        private readonly IPromocodeService _promocodeService;
        public ReceivingPromocodeConsumer(IPromocodeService promocodeService)
        {
            _promocodeService = promocodeService;
        }
        public async Task Consume(ConsumeContext<IReceivingPromocode> context)
        {
           await _promocodeService.GivePromoCodesToCustomerAsync(context.Message);
        }
    }
}