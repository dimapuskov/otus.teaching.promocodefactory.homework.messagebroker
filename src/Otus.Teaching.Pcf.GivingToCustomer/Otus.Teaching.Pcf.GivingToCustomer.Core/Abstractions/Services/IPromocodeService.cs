﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Abstractions.Events;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services
{
    public interface IPromocodeService
    {
        Task<IEnumerable<PromoCode>> GetPromocodesAsync();

        Task<PromoCode> GivePromoCodesToCustomerAsync(IReceivingPromocode request);
    }
}